import './App.css';
import Home from './pages/Home';

function App() {
  return (
    <div className="home">
      <header className='header'>
        <div className='logo'>Noe Osorio (Aqui va su nombre)</div>
        <div className='link'>Home</div>
        <div className='link'>Projects</div>
        <div className='link'>About me</div>
      </header>
      <div>
        <div className='title'>Titulo</div>
        <div className='subtitle'>Subtitulo</div>
        <button className='hire-button'>Hire me</button>
      </div>
      {/* <Home /> */}
    </div>
  );
}

export default App;
